package com.rapidtech.restapi.controller;

import com.rapidtech.restapi.model.CustomerModel;
import com.rapidtech.restapi.model.ResponseModel;
import com.rapidtech.restapi.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/customers")
public class CustomerController {
    private CustomerService service;

    @Autowired
    public CustomerController(CustomerService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Object> getAll() {
        List<CustomerModel> result = service.getAll();
        return ResponseEntity.ok().body(
                new ResponseModel(200, "Successed", result)
        );
    }

    // get by id
    @GetMapping("/{id}")
    public ResponseEntity<Object> getById(@PathVariable("id") Integer id) {
        Optional<CustomerModel> result = service.getById(id);
        return ResponseEntity.ok().body(
                new ResponseModel(200, "Successed", result)
        );
    }

    // save
    @PostMapping()
    public ResponseEntity<Object> saveCustomer(@RequestBody CustomerModel request) throws ParseException {
        Optional<CustomerModel> result = service.save(request);
        return ResponseEntity.ok().body(
                new ResponseModel(200, "Successed", result)
        );
    }

    // update
    @PatchMapping("/{id}")
    public ResponseEntity<Object> updateCustomer(@PathVariable("id") Integer id,
                                                 @RequestBody CustomerModel request){
        Optional<CustomerModel> result = service.update(id, request);
        return ResponseEntity.ok().body(
                new ResponseModel(200, "Successed", result)
        );
    }

    // delete
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Integer id){
        Optional<CustomerModel> result = service.delete(id);
        return ResponseEntity.ok().body(
                new ResponseModel(200, "Successed", result)
        );
    }
}
