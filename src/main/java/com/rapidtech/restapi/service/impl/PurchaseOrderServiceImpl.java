package com.rapidtech.restapi.service.impl;

import com.rapidtech.restapi.entity.PurchaseOrderEntity;
import com.rapidtech.restapi.model.PurchaseOrderDetailModel;
import com.rapidtech.restapi.model.PurchaseOrderModel;
import com.rapidtech.restapi.repository.PurchaseOrderDetailRepository;
import com.rapidtech.restapi.repository.PurchaseOrderRepository;
import com.rapidtech.restapi.service.PurchaseOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
@Slf4j
@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {
    private PurchaseOrderRepository orderRepository;
    private PurchaseOrderDetailRepository detailRepository;
    @Autowired
    public PurchaseOrderServiceImpl(PurchaseOrderRepository orderRepository, PurchaseOrderDetailRepository detailRepository){
        this.orderRepository = orderRepository;
        this.detailRepository = detailRepository;
    }

    @Override
    public Optional<PurchaseOrderModel> save(PurchaseOrderModel model) {
        if(model == null || model.getDetails().isEmpty()) {
            return Optional.empty();
        }
        PurchaseOrderEntity entity = new PurchaseOrderEntity(model);
        entity.addDetailList(model.getDetails());

        try {
            orderRepository.save(entity);
            return Optional.of(model);
        } catch (Exception e){
            log.error("Failed to sava the purchase. Error: {}", e.getMessage());
            return Optional.empty();
        }
    }
}
