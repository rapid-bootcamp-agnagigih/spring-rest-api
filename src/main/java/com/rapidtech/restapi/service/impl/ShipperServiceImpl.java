package com.rapidtech.restapi.service.impl;

import com.rapidtech.restapi.model.PurchaseOrderModel;
import com.rapidtech.restapi.model.ShipperModel;
import com.rapidtech.restapi.repository.PurchaseOrderRepository;
import com.rapidtech.restapi.repository.ShipperRepository;
import com.rapidtech.restapi.service.ShipperService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ShipperServiceImpl implements ShipperService {
    private ShipperRepository repository;
    @Autowired
    public ShipperServiceImpl(ShipperRepository repository){
        this.repository = repository;
    }
    @Override
    public List<ShipperModel> getAll() {
        return this.repository.findAll().stream().map(ShipperModel::new).collect(Collectors.toList());
    }

    @Override
    public Optional<ShipperModel> getById(Long id) {
        return Optional.empty();
    }

    @Override
    public Optional<ShipperModel> save(ShipperModel model) {
        return Optional.empty();
    }

    @Override
    public Optional<ShipperModel> update(Long id, ShipperModel model) {
        return Optional.empty();
    }

    @Override
    public Optional<ShipperModel> delete(Long id) {
        return Optional.empty();
    }
}
