package com.rapidtech.restapi.service.impl;

import com.rapidtech.restapi.entity.CustomerEntity;
import com.rapidtech.restapi.model.CustomerModel;
import com.rapidtech.restapi.repository.CustomerRepository;
import com.rapidtech.restapi.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {
    private CustomerRepository repository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<CustomerModel> getAll() {
        return this.repository.findAll().stream().map(CustomerModel::new)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<CustomerModel> getById(Long id) {
        if(id == 0) {
            return Optional.empty();
        }
        Optional<CustomerEntity> result = this.repository.findById(id);

        return result.map(CustomerModel::new);
    }

    @Override
    public Optional<CustomerModel> save(CustomerModel model){
        if(model == null) {
            return Optional.empty();
        }
        CustomerEntity entity = new CustomerEntity(model);
        try {
            this.repository.save(entity);
            return Optional.of(new CustomerModel(entity));
        } catch (Exception e){
            log.error("Failed to save customer. Error: {}", e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public Optional<CustomerModel> update(Long id, CustomerModel model) {
        if(id == 0) {
            return Optional.empty();
        }
        CustomerEntity result = this.repository.findById(id).orElse(null);
        if(result == null) {
            return Optional.empty();
        }
        // copy property to result
        BeanUtils.copyProperties(model, result);
        try {
            this.repository.save(result);
            return Optional.of(new CustomerModel(result));
        } catch (Exception e) {
            log.error("Failed to update customer. Error: {}", e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public Optional<CustomerModel> delete(Long id) {
        if (id == 0) {
            return Optional.empty();
        }

        CustomerEntity result = this.repository.findById(id).orElse(null);
        if(result == null){
            return Optional.empty();
        }
        try {
            this.repository.delete(result);
            return Optional.of(new CustomerModel(result));
        }catch (Exception e) {
            log.error("Failed to delete customer. Error: {}", e.getMessage());
            return Optional.empty();
        }
    }
}
