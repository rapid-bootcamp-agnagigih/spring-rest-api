package com.rapidtech.restapi.service.impl;

import com.rapidtech.restapi.model.ShipperModel;
import com.rapidtech.restapi.model.SupplierModel;
import com.rapidtech.restapi.repository.ShipperRepository;
import com.rapidtech.restapi.repository.SupplierRepository;
import com.rapidtech.restapi.service.SupplierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SupplierServiceImpl implements SupplierService {
    private SupplierRepository repository;
    @Autowired
    public SupplierServiceImpl(SupplierRepository repository){
        this.repository = repository;
    }
    @Override
    public List<SupplierModel> getAll() {
        return this.repository.findAll().stream().map(SupplierModel::new).collect(Collectors.toList());
    }

    @Override
    public Optional<SupplierModel> getById(Long id) {
        return Optional.empty();
    }

    @Override
    public Optional<SupplierModel> save(SupplierModel model) {
        return Optional.empty();
    }

    @Override
    public Optional<SupplierModel> update(Long id, SupplierModel model) {
        return Optional.empty();
    }

    @Override
    public Optional<SupplierModel> delete(Long id) {
        return Optional.empty();
    }
}
