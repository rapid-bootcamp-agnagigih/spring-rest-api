package com.rapidtech.restapi.service.impl;

import com.rapidtech.restapi.model.EmployeeModel;
import com.rapidtech.restapi.repository.EmployeeRepository;
import com.rapidtech.restapi.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeRepository repository;
    @Autowired
    public EmployeeServiceImpl(EmployeeRepository repository) {
        this.repository = repository;
    }
    @Override
    public List<EmployeeModel> getAll() {
        return this.repository.findAll().stream().map(EmployeeModel::new).collect(Collectors.toList());
    }

    @Override
    public Optional<EmployeeModel> getById(Integer id) {
        return Optional.empty();
    }

    @Override
    public Optional<EmployeeModel> save(EmployeeModel model) {
        return Optional.empty();
    }

    @Override
    public Optional<EmployeeModel> update(Integer id, EmployeeModel model) {
        return Optional.empty();
    }

    @Override
    public Optional<EmployeeModel> delete(Integer id) {
        return Optional.empty();
    }
}
